package it.unibo.oop.lab.nesting2;

import java.util.ArrayList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;
	List<T> listToBeFilled = new ArrayList<>();

	public OneListAcceptable(final List<T> list) {
		this.list = list;
	}
	
	@Override
	public Acceptor<T> acceptor() {
		Acceptor<T> acc = new Acceptor<T>() {
			
			

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(!list.contains(newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
				listToBeFilled.add(newElement);
				
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if(list.size() != listToBeFilled.size()) {
					throw new EndNotAcceptedException();
				}
				
			}
			
			
		};
		return acc;
	}
}
